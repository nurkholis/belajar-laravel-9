<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>

<body>
    <div class="container">
        <div class="d-flex justify-content-between mt-5">
            <h3>{{ isset($edit) ? 'Edit' : 'Tambah' }} Siswa</h3>
            <a href="{{ route('siswa.index') }}" class="btn btn-primary">Kembali</a>
        </div>

        <form method="POST" action="{{ isset($edit) ? route('siswa.update', $edit->id) : route('siswa.store') }}">

            @csrf
            @isset($edit)
                <input type="hidden" name="_method" value="put">    
            @endisset

            <div class="form-group mb-2">
                <label for="nis">NIS</label>
                <input type="number" class="form-control" name="nis"
                    value="{{ isset($edit) ? $edit->nis : old('nis') }}" placeholder="masukkan nis siswa">
                {!! $errors->first('nis', '<small class="text-danger float-right">:message</small>') !!}
            </div>

            <div class="form-group mb-2">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama"
                    value="{{ isset($edit) ? $edit->nama : old('nama') }}" placeholder="masukkan nama siswa">
                {!! $errors->first('nama', '<small class="text-danger float-right">:message</small>') !!}
            </div>

            <div class="form-group mb-2">
                <label for="id_kelas">Kelas</label>
                <select name="id_kelas" id="id_kelas" class="form-control">
                    <option value="">pilih kelas</option>
                    @foreach ($data_kelas as $item)
                        <option value="{{ $item->id }}"
                            @isset($edit)
                            {{ $edit->id_kelas == $item->id ? 'selected' : '' }}
                        @endisset>
                            {{ $item->nama }}</option>
                    @endforeach
                </select>
                {!! $errors->first('id_kelas', '<small class="text-danger float-right">:message</small>') !!}
            </div>
            <br>

            <button class="btn btn-primary float-right" type="submit">Simpan</button>

        </form>


    </div>
</body>

</html>
