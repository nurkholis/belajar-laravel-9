@extends('layouts.master')

@section('content')
    <div class="container">
        <div class="d-flex justify-content-between mt-5 mb-2">
            <h3 class="">
                Data Siswa
            </h3>
            <a class="btn btn-primary" href="{{ route('siswa.create') }}">
                Tambah Siswa
            </a>
        </div>
        @if (Session::has('success'))
            <div class="alert alert-info">{{ Session::get('success') }}</div>
        @endif
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>NIS </th>
                    <th>Nama </th>
                    <th>Kelas</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data_siswa as $item)
                    <tr>
                        <td>{{ $item->nis }}</td>
                        <td>{{ $item->nama }}</td>
                        <td>{{ $item->kelas->nama }}</td>
                        <td>
                            <div class="d-flex">
                                <a class="btn btn-warning btn-sm mr-1" href="{{ route('siswa.edit', $item->id) }}">edit</a>
                                <form action="{{ route('siswa.destroy', $item->id) }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="_method" value="delete">
                                    <button class="btn btn-danger btn-sm" type="submit">hapus</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>
        @if (count($data_siswa) > 0)
            <div class="float-right">
                {{ $data_siswa->links('pagination::bootstrap-4') }}
            </div>
        @endif
    </div>
@endsection
