<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>

<body>
    <div class="container">
        <div class="d-flex justify-content-between mt-5">
            <h3>Form Kelas</h3>
            <a href="{{ route('kelas.index') }}" class="btn btn-primary">Kembali</a>
        </div>


        <form method="POST" action="{{ route('kelas.store') }}">
            @if ($errors->any())
                <p>{{ $errors }}</p>
            @endif
            @csrf
            <div class="form-group mb-2">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="nama" placeholder="masukkan nama kelas">
                {{-- {!! $errors->first('nama', '<small class="text-danger float-right">:message</small>') !!} --}}
                @if ($errors->has('nama'))
                    <small class="text-danger float-right">{{ $errors->first('nama') }}</small>
                @endif
            </div>
            <br>

            <button class="btn btn-primary float-right" type="submit">Simpan</button>

        </form>


    </div>
</body>

</html>
