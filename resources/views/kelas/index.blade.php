<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <div class="d-flex justify-content-between mt-5 mb-2">
            <h3 class="">
                Data Kelas
            </h3>
            <a class="btn btn-primary" href="{{ route('kelas.create') }}">
                Tambah Kelas
            </a>
        </div>
        @if (Session::has('success'))
            <div class="alert alert-info">{{ Session::get('success') }}</div>
        @endif
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ID Kelas </th>
                    <th>Nama Kelas </th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $kelas)
                    <tr>
                        <td>{{ $kelas->id }}</td>
                        <td>{{ $kelas->nama }}</td>
                        <td>
                            <div class="d-flex">
                                <a class="btn btn-warning btn-sm mr-1"
                                    href="{{ route('kelas.edit', $kelas->id) }}">edit</a>
                                <form action="{{ route('kelas.destroy', $kelas->id) }}" method="POST">
                                    @csrf
                                    <input type="hidden" name="_method" value="delete">
                                    <button class="btn btn-danger btn-sm" type="submit">hapus</button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @endforeach

            </tbody>
        </table>
        <div class="float-right">
            {{ $data->links('pagination::bootstrap-4') }}
        </div>
    </div>
</body>

</html>
