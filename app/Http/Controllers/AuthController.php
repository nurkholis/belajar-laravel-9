<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    public function login(Request $request)
    {
        return view('auth.login');
    }

    public function loginSubmit(Request $request)
    {
        // dd(Hash::make('123456'));
        $this->validate($request, [
            'email' => ['required', 'email'],
            'password' => ['required', 'min:3'],
        ]);
        // dd($request->toArray());
        $a = auth('admin')->attempt([
            'email' => $request->email,
            'password' => $request->password,
        ]);
        if($a){
            return redirect()->route('siswa.index');
        }
        return redirect()->back()->with(['error' => 'email atau password tidak cocok dengan akun manapun']);
    }

    public function logout()
    {
        auth()->logout();
        session()->flush();
        return redirect()->route('home');
    }
}
