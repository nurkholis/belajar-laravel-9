<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $result = DB::raw('select * from kelas')->get();
        // $result = DB::table('kelas')
        //     ->get();
        $result = Kelas::orderBy('id', 'desc')
            ->paginate(3);
        // dd($result);
        return view('kelas.index', [
            'data' => $result,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('kelas.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nama' => ['required']
        ]);

        $nama = $request->nama;
        $kelas = new Kelas();

        $result = $kelas
            ->insert([
                'nama' => $nama,
            ]);

        return redirect()
            ->route('kelas.index')
            ->with(['success' => 'berhasil menambah data kelas']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kelas = Kelas::find($id);
        return view('kelas.edit', [
            'edit' => $kelas,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nama' => ['required', 'min:3']
        ]);

        $nama = $request->nama;
        $kelas = new Kelas();

        $result = $kelas
            ->where('id', '=', $id)
            ->update([
                'nama' => $nama,
            ]);

        return redirect()
            ->route('kelas.index')
            ->with(['success' => 'berhasil mengedit data kelas']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kelas = new Kelas();

        $result = $kelas
            ->where('id', '=', $id)
            ->delete();

        return redirect()
            ->route('kelas.index')
            ->with(['success' => 'berhasil menghapus data kelas']);
    }
}
