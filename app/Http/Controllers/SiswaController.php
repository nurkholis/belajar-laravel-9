<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use App\Models\Siswa;
use Illuminate\Http\Request;

class SiswaController extends Controller
{
    private $siswa, $kelas;

    public function __construct()
    {
        $this->siswa = new Siswa();
        $this->kelas = new Kelas();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $siswa = $this->siswa
        // ->select('siswa.*', 'kelas.nama as nama_kelas')
        // ->join('kelas', 'kelas.id', '=', 'siswa.id_kelas')
        // ->get();

        $siswa = $this->siswa
            ->with(['kelas'])
            ->paginate(5);

        return view('siswa.index', [
            'data_siswa' =>  $siswa,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kelas = $this->kelas->get();
        return view('siswa.form', [
            'data_kelas' => $kelas,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nis' => ['required', 'numeric'],
            'nama' => ['required'],
            'id_kelas' => ['required'],
        ]);

        $this->siswa->create([
            'nis' => $request->nis,
            'nama' => $request->nama,
            'id_kelas' => $request->id_kelas,
        ]);

        return redirect()
            ->route('siswa.index')
            ->with([
                'success' => 'berhasil menyimpan data siswa',
            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit = $this->siswa->find($id);
        $kelas = $this->kelas->get();

        return view('siswa.form', [
            'data_kelas' => $kelas,
            'edit' => $edit,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nis' => ['required', 'numeric'],
            'nama' => ['required'],
            'id_kelas' => ['required'],
        ]);

        $this->siswa
            ->where('id', '=', $id)
            ->update([
                'nis' => $request->nis,
                'nama' => $request->nama,
                'id_kelas' => $request->id_kelas,
            ]);

        return redirect()
            ->route('siswa.index')
            ->with([
                'success' => 'berhasil mengupdate data siswa',
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->siswa->find($id)->delete();
        return redirect()
            ->route('siswa.index')
            ->with([
                'success' => 'berhasil menghapus data siswa',
            ]);
    }
}
